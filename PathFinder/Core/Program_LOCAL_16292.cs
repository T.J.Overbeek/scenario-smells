﻿using System;
using System.Collections.Generic;
using AssetPackage;
using ScenarioLanguage;

namespace PathFinder
{
    class Program
    {
        private static bool running = true;
        private enum TestCases { Test1A, Test2, Test3, Test3S, Test3M, Test4S, Test5S, None};
        private static TestCases testCase = TestCases.None;

        static void Main(string[] args)
        {
            if (testCase == TestCases.None)
            {
                while (running)
                {
                    ReadInput();
                }
            }
            else
            {
                switch(testCase)
                {
                    case TestCases.Test1A:
                        TestCase1A();
                        break;
                    //case TestCases.Test1B:
                        //TestCase1B();
                       // break;
                    case TestCases.Test2:
                        TestCase2();
                        break;
                    case TestCases.Test3:
                        TestCase3();
                        break;
                    case TestCases.Test3S:
                        TestCase3S();
                        break;
                    case TestCases.Test3M:
                        TestCase3M();
                        break;
                    case TestCases.Test4S:
                        TestCase4S();
                        break;
                    case TestCases.Test5S:
                        TestCase5S();
                        break;
                }
                ReadInput();
            }
        }

        private static void ReadInput()
        {
            string input = Console.ReadLine();
            string[] inputSplit = input.Split(' ');
            switch (inputSplit[0])
            {
                case "scenario":
                    Controller.InitiaizeScenario(inputSplit[1]);
                    if(inputSplit.Length > 2)
                        DetermineEvaluationParameters(inputSplit);
                    Controller.CreateSearchTree("Total Score");
                    break;
                case "complete":
                    Controller.InitiaizeScenario(inputSplit[1]);
                    Controller.CreateCompleteTree("First Parameter");
                    break;
                case "evaluation":
                    CompleteEvaluation(inputSplit);
                    break;
                case "debug":
                    if (inputSplit[1] == "true")
                        OutputController.DebugMode = true;
                    else
                        OutputController.DebugMode = false;
                    break;
                case "close":
                    running = false;
                    break;
            }
        }

        private static void CompleteEvaluation(string[] input)
        {
            string scenarioName = input[1];
            Controller.InitiaizeScenario(scenarioName);

            DetermineEvaluationParameters(input);
            Controller.CreateSearchTree("Total Score");

            for(int i = 0; i < Controller.OriginalParameters.Length; i++)
            {
                Controller.ChangeEvaluation(Controller.OriginalParameters[i].Name);
                Controller.CreateSearchTree(Controller.OriginalParameters[i].Name);
            }
        }

        private static void DetermineEvaluationParameters(string[] inputSplit)
        {
            if (inputSplit.Length == 2)
            {
                Controller.ChangeEvaluation(inputSplit[2]);
            }
            else
            {
                int count = inputSplit.Length - 2;
                if (count%2 != 0)
                    OutputController.Error("ERROR: not the right amount of parameters");
                string[] parameters = new string[count];
                for(int i = 0; i < count; i++)
                {
                    parameters[i] = inputSplit[i + 2];
                }
                Controller.ChangeEvaluation(parameters);
            }
        }

        //private static void TestCase1A()
        //{
        //    OutputController.Output("Running Test Case 1A:");
        //    OutputController.DebugMode = true;
        //    Controller.ControlString = "C101 P103 C104 P107 C107 P111 C110 P201 C201 P204 C204 P208 C207 P302 C302 P306 C305 P310 C308 ";
        //    Controller.InitiaizeScenario("Test1");
        //    Controller.CreateSearchTree();
        //}

        //private static void TestCase1B()
        //{
        //    OutputController.Output("Running Test Case 1B:");
        //    OutputController.DebugMode = false;
        //    Controller.ControlString = "C101 P103 C104 P107 C107 P111 C110 P201 C201 P204 C204 P208 C207 P302 C302 P306 C305 P310 C308 ";
        //    Controller.InitiaizeScenario("Test1");
        //    Controller.CreateSearchTree();
        //}

        private static void TestCase1A()
        {
            OutputController.Output("Running Test Case 1A (simple, preconditions, no subject switches):");
            OutputController.DebugMode = false;
            Controller.ControlString = "C101 P103 C104 P107 C107 P111 C110 P401 C401 P404 C404 P409 C408 P301 C301 P304 C304 P308 C307 P201 C201 P204 C204 P208 C207 P502 C502 P506 C505 ";
            Controller.InitiaizeScenario("Test1A");
            Controller.CreateSearchTree("Total Score");
        }

        private static void TestCase2()
        {
            OutputController.Output("Running Test Case 2:");
            OutputController.DebugMode = false;
            Controller.ControlString = "C101 P103 C104 P107 C107 P111 C110 P201 C201 P204 C204 P208 C207 P301 C301 P304 C304 P308 C307 P401 C401 P404 C404 P408 C407 P502 C502 P506 C505 P510 C508 ";
            Controller.InitiaizeScenario("Test2");
            Controller.CreateSearchTree("Total Score");
        }

        private static void TestCase3()
        {
            OutputController.Output("Running Test Case 3:");
            OutputController.DebugMode = false;
            Controller.ControlString = "C101 P103 C104 P107 C107 P111 C110 P501 C501 P504 C504 P508 C507 P401 C401 P404 C404 P408 C407 P301 C301 P304 C304 P308 C307 P201 C201 P204 C204 P208 C207 P602 C602 P606 C605 P610 C608 ";
            Controller.InitiaizeScenario("Test3");
            Controller.CreateSearchTree("Total Score");
        }

        private static void TestCase3S()
        {
            OutputController.Output("Running Test Case 3S:");
            OutputController.DebugMode = false;
            Controller.ControlString = "C101 P103 C104 P107 C107 P111 C110 P401 C401 P404 C404 P408 C407 P301 C301 P304 C304 P308 C307 P201 C201 P204 C204 P208 C207 P602 C602 P606 C605 P610 C608 ";
            Controller.InitiaizeScenario("Test3S");
            Controller.CreateSearchTree("Total Score");
        }

        private static void TestCase3M()
        {
            OutputController.Output("Running Test Case 3M");
            OutputController.Output(" ");
            OutputController.DebugMode = false;
            Controller.InitiaizeScenario("Test3M");

            OutputController.Output("Running Total Score: ");
            Controller.ChangeEvaluation(new string[] { "Score1", "1", "Score2", "1" });
            Controller.ControlString = "C101 P103 C104 P107 C107 P111 C110 P401 C401 P404 C404 P408 C407 P301 C301 P304 C304 P308 C307 P201 C201 P204 C204 P208 C207 P602 C602 P606 C605 P609 C608";
            Controller.CreateSearchTree("Total Score");

            OutputController.Output(" ");

            OutputController.Output("Running Score 1:");
            Controller.ChangeEvaluation("Score1");
            Controller.ControlString = "C101 P103 C104 P107 C107 P111 C110 P401 C401 P404 C404 P408 C407 P301 C301 P304 C304 P308 C307 P201 C201 P204 C204 P208 C207 P602 C602 P606 C605 P610 C608";
            Controller.CreateSearchTree("Score 1");

            OutputController.Output(" ");

            OutputController.Output("Running Score 2:");
            Controller.ChangeEvaluation("Score2");
            Controller.ControlString = "C101 P101 C102 P104 C105 P108 C108 P401 C401 P404 C404 P408 C407 P301 C301 P304 C304 P308 C307 P201 C201 P204 C204 P208 C207 P601 C601 P604 C604 P609 C608";
            Controller.CreateSearchTree("Score 2");
        }

        private static void TestCase4S()
        {
            OutputController.Output("Running Test Case 4S:");
            OutputController.DebugMode = false;
            Controller.ControlString = "C101 P103 C104 P107 C107 P111 C110 P401 C401 P404 C404 P301 C301 P304 C304 P308 C307 P201 C201 P204 C204 P208 C207 P408 C407 P602 C602 P606 C605 P610 C608 ";
            Controller.InitiaizeScenario("Test4S");
            Controller.CreateSearchTree("Total Score");
        }

        private static void TestCase5S()
        {
            OutputController.Output("Running Test Case 5S:");
            OutputController.DebugMode = false;
            Controller.ControlString = "C101 P103 C104 P107 C107 P111 C110 P401 C401 P404 C404 P301 C301 P304 C304 P308 C307 P201 C201 P204 C204 P208 C207 P408 C407 P602 C602 P606 C605 P610 C608 P502 C502 P506 C505 P510 C508 P701 ";
            Controller.InitiaizeScenario("Test5S");
            Controller.ChangeEvaluation(new string[] { "Score1", "1", "Score2", "2" });
            Controller.CreateSearchTree("Total Score");
        }
    }
}
