﻿using System;
using System.Collections.Generic;
using AssetPackage;
using ScenarioLanguage;

namespace PathFinder
{
    class Program
    {
        private static bool running = true;
        private enum TestCases { Test1, Test1M, None};
        private static TestCases testCase = TestCases.Test1;

        static void Main(string[] args)
        {
            if (testCase == TestCases.None)
            {
                while (running)
                {
                    ReadInput();
                }
            }
            else
            {
                switch(testCase)
                {
                    case TestCases.Test1:
                        TestCase1();
                        break;
                    case TestCases.Test1M:
                        TestCase1M();
                        break;
                }
                ReadInput();
            }
        }

        private static void ReadInput()
        {
            string input = Console.ReadLine();
            string[] inputSplit = input.Split(' ');
            switch (inputSplit[0])
            {
                case "scenario":
                    Controller.InitiaizeScenario(inputSplit[1]);
                    if(inputSplit.Length > 2)
                        DetermineEvaluationParameters(inputSplit);
                    Controller.CreateSearchTree("Total Score");
                    break;
                case "complete":
                    Controller.InitiaizeScenario(inputSplit[1]);
                    Controller.CreateCompleteTree("First Parameter");
                    break;
                case "evaluation":
                    CompleteEvaluation(inputSplit);
                    break;
                case "debug":
                    if (inputSplit[1] == "true")
                        OutputController.DebugMode = true;
                    else
                        OutputController.DebugMode = false;
                    break;
                case "close":
                    running = false;
                    break;
            }
        }

        private static void CompleteEvaluation(string[] input)
        {
            string scenarioName = input[1];
            Controller.InitiaizeScenario(scenarioName);

            DetermineEvaluationParameters(input);
            Controller.CreateSearchTree("Total Score");

            for(int i = 0; i < Controller.OriginalParameters.Length; i++)
            {
                Controller.ChangeEvaluation(Controller.OriginalParameters[i].Name);
                Controller.CreateSearchTree(Controller.OriginalParameters[i].Name);
            }
        }

        private static void DetermineEvaluationParameters(string[] inputSplit)
        {
            if (inputSplit.Length == 2)
            {
                Controller.ChangeEvaluation(inputSplit[2]);
            }
            else
            {
                int count = inputSplit.Length - 2;
                if (count%2 != 0)
                    OutputController.Error("ERROR: not the right amount of parameters");
                string[] parameters = new string[count];
                for(int i = 0; i < count; i++)
                {
                    parameters[i] = inputSplit[i + 2];
                }
                Controller.ChangeEvaluation(parameters);
            }
        }

        private static void TestCase1()
        {
            OutputController.Output("Running Test Case 1:");
            OutputController.DebugMode = false;
            string controlString = "C101 P103 C104 P107 C107 P111 C110 P401 C401 P404 C404 P408 C407 P301 C301 P304 C304 P308 C307 P201 C201 P204 C204 P208 C207 P602 C602 P606 C605 P610 C608";
            Controller.OptimalPathEvaluation("Test3M", controlString);
        }

        private static void TestCase1M()
        {
            OutputController.Output("Running Test Case 1M");
            OutputController.Output(" ");
            OutputController.DebugMode = false;
            Controller.InitiaizeScenario("Test3M");

            OutputController.Output("Running Total Score: ");
            Controller.ChangeEvaluation(new string[] { "Score1", "1", "Score2", "1" });
            Controller.ControlString = "C101 P103 C104 P107 C107 P111 C110 P401 C401 P404 C404 P408 C407 P301 C301 P304 C304 P308 C307 P201 C201 P204 C204 P208 C207 P602 C602 P606 C605 P609 C608";
            Controller.CreateSearchTree("Total Score");

            OutputController.Output(" ");

            OutputController.Output("Running Score 1:");
            Controller.ChangeEvaluation("Score1");
            Controller.ControlString = "C101 P103 C104 P107 C107 P111 C110 P401 C401 P404 C404 P408 C407 P301 C301 P304 C304 P308 C307 P201 C201 P204 C204 P208 C207 P602 C602 P606 C605 P610 C608";
            Controller.CreateSearchTree("Score 1");

            OutputController.Output(" ");

            OutputController.Output("Running Score 2:");
            Controller.ChangeEvaluation("Score2");
            Controller.ControlString = "C101 P101 C102 P104 C105 P108 C108 P401 C401 P404 C404 P408 C407 P301 C301 P304 C304 P308 C307 P201 C201 P204 C204 P208 C207 P601 C601 P604 C604 P609 C608";
            Controller.CreateSearchTree("Score 2");
        }
    }
}
