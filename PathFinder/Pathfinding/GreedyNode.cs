﻿using System;
using System.Collections.Generic;
using ScenarioLanguage;

namespace PathFinder
{
    class GreedyNode : Node
    {
        protected List<Node> children = new List<Node>();

        public GreedyNode(SearchTree tree, statementType statement, Path path, GreedyNode parent = null, int subjectIndex = 0) : base(tree, statement, path, parent, subjectIndex)
        {
        }

        public override void FindChildren()
        {
            base.FindChildren();

            float score = -10000;
            for(int i = 0; i < children.Count; i++)
            {
                if (children[i] != null && CheckPreconditions(children[i].statement))
                {
                    float test = tree.EvaluateScore(children[i]);
                    if (tree.EvaluateScore(children[i]) > score)
                        score = tree.EvaluateScore(children[i]);
                }
            }
            for (int i = 0; i < children.Count; i++)
            {
                if (children[i] != null)
                {
                    if (tree.EvaluateScore(children[i]) == score && CheckPreconditions(children[i].statement))
                        children[i].FindChildren();
                }
            }
        }

        protected override void FindWithinSubject()
        {
            int childCount = statement.responses.Length;
            for (int i = 0; i < childCount; i++)
            {
                string childID = statement.responses[i].idref;
                children.Add(CreateChild(tree.FindStatement(childID, subjectIndex), path, subjectIndex));
            }
        }

        protected override void FindOutsideSubject()
        {
            List<SubjectStart> nextSubjects = path.AvailableSubjects;
            if (nextSubjects.Count == 0)
            {
                path = path.FindNextSubjects(tree.Scenario);
                nextSubjects = path.AvailableSubjects;
            }
            int childCount = 0;
            for (int i = 0; i < nextSubjects.Count; i++)
            {
                childCount += nextSubjects[i].ChildCount;
            }

            int counter = 0;
            for (int i = 0; i < nextSubjects.Count; i++)
            {
                Path newPath = path.SelectSubject(nextSubjects[i].ID);
                for (int j = 0; j < nextSubjects[i].ChildCount; j++)
                {
                    string childID = nextSubjects[i].StartStatements[j].id;
                    children.Add(CreateChild(tree.FindStatement(childID, nextSubjects[i].ID), newPath, nextSubjects[i].ID));
                    counter++;
                }
            }
        }

        protected override Node CreateChild(statementType childStatement, Path childPath, int childSubjectIndex)
        {
            return new GreedyNode(tree, childStatement, childPath, this, childSubjectIndex);
        }
    }
}
