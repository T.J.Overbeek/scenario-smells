﻿using System;
using System.Collections.Generic;
using ScenarioLanguage;
namespace PathFinder
{
    abstract partial class Node
    {
        public statementType statement;
        protected string id;
        protected string text;
        public Parameter[] parameters;

        public string history;
        protected Path path;
        protected int subjectIndex;
        protected SearchTree tree;
        protected int pathLength;

        public Node(SearchTree tree, statementType statement, Path path, Node parent = null, int subjectIndex = 0)
        {
            this.tree = tree;
            this.statement = statement;
            this.path = path;
            this.subjectIndex = subjectIndex;

            id = statement.id;
            text = statement.text.Value;
            OutputController.NodeCreation(id, text);

            SetParentParemeters(parent);
            CalculateScore();

            if (parent == null)
                pathLength = 1;
            else
                pathLength = parent.PathLength + 1;
        }

        protected void SetParentParemeters(Node parent)
        {
            if (parent == null)
            {
                history = id + " ";
                parameters = new Parameter[tree.OriginalParameters.Length];
                for (int i = 0; i < tree.OriginalParameters.Length; i++)
                {
                    parameters[i] = tree.OriginalParameters[i].Clone();
                }
            }
            else
            {
                history = parent.history + id + " ";
                parameters = new Parameter[parent.parameters.Length];
                for (int i = 0; i < parent.parameters.Length; i++)
                {
                    parameters[i] = parent.parameters[i].Clone();
                }

            }
        }

        protected void CalculateScore()
        {
            for (int j = 0; j < statement.parameterEffects.userDefined.Length; j++)
            {
                parameterEffect effect = statement.parameterEffects.userDefined[j];
                for (int k = 0; k < parameters.Length; k++)
                {
                    if (parameters[k].ID == effect.idref)
                    {
                        parameters[k].ChangeValue(effect);
                        BruteSearchTree bruteTree = tree as BruteSearchTree;
                        if(bruteTree != null)
                            bruteTree.reportParameter(subjectIndex, k);
                        break;
                    }
                }
            }
        }

        public virtual void FindChildren()
        {
            if (statement.end)
            {
                tree.ReportEndNode(this);
                return;
            }

            if (statement is dialogueComputerStatement && (statement as dialogueComputerStatement).allowDialogueEnd)
            {
                tree.ReportEndNode(this);
            }

            if (statement.responses.Length == 0)
            {
                FindOutsideSubject();
            }
            else if (statement is dialogueComputerStatement && (statement as dialogueComputerStatement).allowInterleave)
            {
                List<statementType> followUpStatements = new List<statementType>();
                int childCount = statement.responses.Length;
                for (int i = 0; i < childCount; i++)
                {
                    string childID = statement.responses[i].idref;
                    followUpStatements.Add(tree.FindStatement(childID, subjectIndex));
                }
                SubjectStart subjectRestart = new SubjectStart(subjectIndex, followUpStatements);
                path = path.AddSubject(subjectRestart);
                FindOutsideSubject();
            }
            else
                FindWithinSubject();
        }

        protected abstract Node CreateChild(statementType childStatement, Path childPath, int childSubjectIndex);
        protected abstract void FindOutsideSubject();
        protected abstract void FindWithinSubject();

        public Parameter Parameter(int index)
        {
            return parameters[index];
        }

        public string Text
        {
            get { return text; }
        }

        public int PathLength
        {
            get { return pathLength; }
        }
    }
}