﻿using System;
using System.Collections.Generic;
using ScenarioLanguage;

namespace PathFinder
{
    abstract partial class Node
    {
        protected bool CheckPreconditions(statementType childStatement)
        {
            singleConditionType preconditions = childStatement.preconditions;
            if (preconditions != null)
            {
                return EvaluateCondition(preconditions);
            }
            return true;
        }

        protected bool EvaluateCondition(singleConditionType condition)
        {
            switch (condition.ItemElementName)
            {
                case ItemChoiceType.condition:
                    return EvaluateSingleCondition(condition.Item as conditionsTypeCondition);
                case ItemChoiceType.and:
                    return EvaluateAndCondition(condition.Item as conditionsTypeAndCondition);
                case ItemChoiceType.or:
                    return EvaluateOrCondition(condition.Item as conditionsTypeOrCondition);
                default:
                    OutputController.Error("Precondition type not recognized", text);
                    return false;
            }
        }

        protected bool EvaluateCondition(object condition)
        {
            conditionsTypeCondition singleCondition = condition as conditionsTypeCondition;
            if (singleCondition != null)
                return EvaluateSingleCondition(singleCondition);

            conditionsTypeAndCondition andCondition = condition as conditionsTypeAndCondition;
            if (andCondition != null)
                return EvaluateAndCondition(andCondition);

            conditionsTypeOrCondition orCondition = condition as conditionsTypeOrCondition;
            if (orCondition != null)
                return EvaluateOrCondition(orCondition);

            OutputController.Error("Precondition type not recognized", text);
            return false;
        }

        protected bool EvaluateSingleCondition(conditionsTypeCondition condition)
        {
            for (int i = 0; i < parameters.Length; i++)
            {
                if (parameters[i].ID == condition.idref)
                {
                    BruteSearchTree bruteTree = tree as BruteSearchTree;
                    if(bruteTree != null)
                        bruteTree.reportPrecondition(subjectIndex, i);
                    return parameters[i].CompareValue(condition);
                }
            }

            OutputController.Error("Precondtion parameter not found", text);
            return false;
        }

        protected bool EvaluateAndCondition(conditionsTypeAndCondition condition)
        {
            bool result = true;
            for (int i = 0; i < condition.Items.Length; i++)
            {
                if (!EvaluateCondition(condition.Items[i]))
                    result = false;
            }
            return result;
        }

        protected bool EvaluateOrCondition(conditionsTypeOrCondition condition)
        {
            for (int i = 0; i < condition.Items.Length; i++)
            {
                if (EvaluateCondition(condition.Items[i]))
                    return true;
            }
            return false;
        }
    }
}