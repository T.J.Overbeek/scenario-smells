﻿using System;
using System.Collections.Generic;
using ScenarioLanguage;

namespace PathFinder
{
    class SubjectStart
    {
        private int id;
        private List<statementType> startStatements;

        public SubjectStart(int id, List<statementType> startStatements)
        {
            this.id = id;
            this.startStatements = startStatements;
        }

        public int ID
        {
            get { return id; }
        }

        public List<statementType> StartStatements
        {
            get { return startStatements; }
        }

        public int ChildCount
        {
            get { return startStatements.Count; }
        }
    }
}
