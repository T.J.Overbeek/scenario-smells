﻿using System;
using System.Collections.Generic;
using ScenarioLanguage;
using System.Diagnostics;

namespace PathFinder
{
    class GreedySearchTree : SearchTree
    {

        public GreedySearchTree(Scenario scenario, Evaluation[] evaluation) : base(scenario, evaluation)
        {
        }

        public override void BuildTree()
        {
            stopwatch = Stopwatch.StartNew();
            OutputController.Debug("\nStarted building Search Tree");
            CreateStartNodes();
            OutputController.Debug("Finished building Search Tree \n");
            stopwatch.Stop();
        }

        public override void ReportEndNode(Node endNode)
        {
            if (bestNode == null)
            {
                bestNode = endNode;
                allBestNodes = new List<Node>();
                allBestNodes.Add(endNode);
            }

            else
            {
                allBestNodes.Add(endNode);
                if (EvaluateScore(endNode) < EvaluateScore(bestNode))
                {
                    bestNode = endNode;
                }
            }
        }

        protected override void CreateStartNodes()
        {
            Path path = new Path();
            path.CreateEmptyPath();

            string[] startIDs = Scenario.FindStartIDs();
            List<statementType> followUpStatements = new List<statementType>();
            List<GreedyNode> children = new List<GreedyNode>();

            for (int i = 0; i < startIDs.Length; i++)
            {
                followUpStatements.Add(FindStatement(startIDs[i], 0));
                children.Add(new GreedyNode(this, FindStatement(startIDs[i], 0), path));
            }

            float score = -10000;
            for (int i = 0; i < children.Count; i++)
            {
                float test = EvaluateScore(children[i]);
                if (EvaluateScore(children[i]) > score)
                    score = EvaluateScore(children[i]);
            }

            for (int i = 0; i < children.Count; i++)
            {
                if (children[i] != null)
                {
                    if (EvaluateScore(children[i]) == score)
                        children[i].FindChildren();
                }
            }
        }
    }
}