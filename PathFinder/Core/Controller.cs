﻿using System;
using System.Collections.Generic;
using ScenarioLanguage;
using System.IO;
using System.Xml.Serialization;

namespace PathFinder
{
    static class Controller
    {
        private static Scenario scenario;
        private static Evaluation[] evaluation;
        private static string controlString;
        private static string greedyControlString;
        private static BruteSearchTree mainSearchTree;

        public static void ShortEvaluation(string[] parameters, float[] weight)
        {
            FileWriter.NewFile();

            ChangeEvaluation(parameters, weight);
            OutputController.Output("Evaluating Total Score");
            OutputController.Output("");
            FileWriter.NewParameter("BestScore");

            BruteSearchTree bruteTree = CreateSearchTree();
        }

        public static void CompleteEvaluation(string[] parameters, float[] weight, bool gready = true)
        {
            FileWriter.NewFile();

            ChangeEvaluation(parameters, weight);
            OutputController.Output("Evaluating Total Score");
            OutputController.Output("");
            FileWriter.NewParameter("BestScore");
            Evaluate(false, gready);

            for (int i = 0; i < parameters.Length; i++)
            {
                ChangeEvaluation(parameters[i]);
                OutputController.Output("Evaluating " + parameters[i]);
                OutputController.Output("");
                FileWriter.NewParameter(parameters[i]);
                Evaluate(true, gready);
            }

            FileWriter.OutputScenarioSmells("Correlation");
            ChangeEvaluation(parameters, weight);
            Correlation();

            FileWriter.OutputScenarioSmells("Subject Monotone");
            SubjectMonotone();
        }

        private static void Evaluate(bool singleParameter, bool gready = true)
        {
            BruteSearchTree bruteTree = CreateSearchTree();
            if (!singleParameter)
                mainSearchTree = bruteTree;

            if (gready)
            {
                GreedySearchTree greedyTree = CreateGreedySearchTree();
                bool smellOne = false;
                if (greedyTree.BestScore < bruteTree.BestScore)
                    smellOne = true;
                FileWriter.OutputSmellOne(smellOne);
            }

            bool smellTwo = false;
            float[] scores = bruteTree.LongestPathScores;
            for (int i = 0; i < scores.Length; i++)
                if (bruteTree.BestScore == scores[i])
                    smellTwo = true;
            FileWriter.OutputsmellTwo(smellTwo);

            bool smellThree = false;
            scores = bruteTree.ShortestPathScores;
            for (int i = 0; i < scores.Length; i++)
                if (bruteTree.WorstScore == scores[i])
                    smellTwo = true;
            FileWriter.OutputsmellThree(smellThree);

            if (!singleParameter)
                return;

            Parameter evaluatedParameter = scenario.FindParameter(evaluation[0].ID);
            if(evaluatedParameter.MinAndMax)
            {
                bool smellFour = false;
                if (bruteTree.WorstScore > evaluatedParameter.MinValue)
                    smellFour = true;
                FileWriter.OutputsmellFour(smellFour);
                bool smellFive = false;
                if (bruteTree.BestScore < evaluatedParameter.MaxValue)
                    smellFive = true;
                FileWriter.OutputsmellFive(smellFive);
            }
        }

        public static BruteSearchTree CreateSearchTree()
        {
            OutputController.Output("Calculating Best Score");
            BruteSearchTree searchTree = new BruteSearchTree(scenario, evaluation);
            searchTree.BuildTree();
            List<statementType[]> bestPaths = searchTree.AllBestPaths;
            OutputController.OutputBestPath(bestPaths[0], searchTree.BestScore, searchTree.CalculationTime);
            FileWriter.OutputBestPath(bestPaths, searchTree.BestScore, "Best Score", searchTree);
            OutputController.Output("");
            return searchTree;
        }

        public static GreedySearchTree CreateGreedySearchTree()
        {
            OutputController.Output("Calculating Always Best Option");
            GreedySearchTree greedyTree = new GreedySearchTree(scenario, evaluation);
            greedyTree.BuildTree();
            List<statementType[]> bestPaths = greedyTree.AllBestPaths;
            OutputController.OutputBestPath(bestPaths[0], greedyTree.BestScore, greedyTree.CalculationTime);
            FileWriter.OutputBestPath(bestPaths, greedyTree.BestScore, "Always Best Option", greedyTree);
            OutputController.Output("");
            return greedyTree;
        }

        public static void SubjectMonotone()
        {
            for (int i = 0; i < scenario.OriginalParameters.Length; i++)
            {
                bool monotone = mainSearchTree.SubjectMonotone(i);
                string output = scenario.OriginalParameters[i].Name + " is ";
                if (!monotone)
                    output += "not ";
                output += "subject monotone";
                OutputController.Output(output);
                FileWriter.WriteLine(output);
            }
        }

        public static bool Correlation()
        {
            OutputController.Output("Calculating Correlation");
            CorrelationSearchTree correlationTree = new CorrelationSearchTree(scenario, evaluation);
            correlationTree.BuildTree();
            for(int i = 0; i < evaluation.Length - 1; i++)
            {
                for(int j = i + 1; j < evaluation.Length; j++)
                {
                    int firstIndex = evaluation[i].Index;
                    int secondIndex = evaluation[j].Index;
                    double correlation = correlationTree.CalculateCorrelation(firstIndex, secondIndex);
                    OutputController.Output("Correlation " + OriginalParameters[firstIndex].Name + " " + OriginalParameters[secondIndex].Name + ": " + correlation);
                    FileWriter.WriteLine("Correlation between " + OriginalParameters[firstIndex].Name + " and " + OriginalParameters[secondIndex].Name + ": " + correlation);
                }
            }
            return true;
        }

        public static void BestPathTest(string controlString)
        {
            SearchTree searchTree = new BruteSearchTree(scenario, evaluation);
            searchTree.BuildTree();
            List<statementType[]> bestPaths = searchTree.AllBestPaths;
            OutputController.OutputBestPath(bestPaths[0], searchTree.BestScore, searchTree.CalculationTime);
            string testString = CreateControlString(bestPaths[0]);

            if (controlString.Equals(testString))
                OutputController.Output("Answer Correct!");
            else
                OutputController.Output("Anser Incorrect!");
        }

        public static void WorstPathTest(string controlString)
        {
            SearchTree searchTree = new BruteSearchTree(scenario, evaluation);
            BruteSearchTree bruteTree = searchTree as BruteSearchTree;
            searchTree.BuildTree();
            List<statementType[]> worstPaths = bruteTree.AllWorstPaths;
            OutputController.OutputBestPath(worstPaths[0], bruteTree.WorstScore, searchTree.CalculationTime);
            string testString = CreateControlString(worstPaths[0]);

            if (controlString.Equals(testString))
                OutputController.Output("Answer Correct!");
            else
                OutputController.Output("Anser Incorrect!");
        }

        public static void LongestPathTest(int length)
        {
            SearchTree searchTree = new BruteSearchTree(scenario, evaluation);
            BruteSearchTree bruteTree = searchTree as BruteSearchTree;
            searchTree.BuildTree();
            List<statementType[]> longestPaths = bruteTree.AllLongestPaths;
            OutputController.OutputBestPath(longestPaths[0], bruteTree.LongestPathScores[0], searchTree.CalculationTime);

            if (bruteTree.LongestPathLength == length)
                OutputController.Output("Answer Correct!");
            else
                OutputController.Output("Anser Incorrect!");
        }

        public static void ShortestPathTest(int length)
        {
            SearchTree searchTree = new BruteSearchTree(scenario, evaluation);
            BruteSearchTree bruteTree = searchTree as BruteSearchTree;
            searchTree.BuildTree();
            List<statementType[]> shortestPaths = bruteTree.AllShortestPaths;
            OutputController.OutputBestPath(shortestPaths[0], bruteTree.ShortestPath, searchTree.CalculationTime);

            if (bruteTree.ShortestPath == length)
                OutputController.Output("Answer Correct!");
            else
                OutputController.Output("Anser Incorrect!");
        }

        public static void GreedyPathTest(string controlString)
        {
            SearchTree searchTree = new GreedySearchTree(scenario, evaluation);
            searchTree.BuildTree();
            List<statementType[]> bestPaths = searchTree.AllBestPaths;
            OutputController.OutputBestPath(bestPaths[0], searchTree.BestScore, searchTree.CalculationTime);
            string testString = CreateControlString(bestPaths[0]);
            if (controlString.Equals(testString))
                OutputController.Output("Answer Correct!");
            else
                OutputController.Output("Anser Incorrect!");
        }

        public static void InitializeScenario(string name)
        {
            XmlSerializer ser = new XmlSerializer(typeof(ScenarioLanguage.scenario));
            StreamReader reader = new StreamReader(@name);
            ScenarioLanguage.scenario scenarioXML = (ScenarioLanguage.scenario)ser.Deserialize(reader);
            reader.Close();
            scenario = new Scenario(scenarioXML);
            scenario.InitializeEvaluation();
        }

        public static void ChangeEvaluation(string parameterName)
        {
            evaluation = new Evaluation[1];
            for(int i = 0; i < scenario.OriginalParameters.Length; i++)
            {
                if (scenario.OriginalParameters[i].Name == parameterName)
                    evaluation[0] = new Evaluation(scenario.OriginalParameters[i].ID, i);
            }
        }

        public static void ChangeEvaluation(string[] parameters)
        {
            evaluation = new Evaluation[parameters.Length / 2];
            for(int i = 0; i < evaluation.Length; i++)
            {
                for (int j = 0; j < scenario.OriginalParameters.Length; j++)
                {
                    if (scenario.OriginalParameters[j].Name == parameters[i * 2])
                        evaluation[i] = new Evaluation(scenario.OriginalParameters[j].ID, j, int.Parse(parameters[i*2+1]));
                }
            }
        }

        public static void ChangeEvaluation(string[] parameters, float[] weights)
        {
            evaluation = new Evaluation[parameters.Length];
            for (int i = 0; i < evaluation.Length; i++)
            {
                for (int j = 0; j < scenario.OriginalParameters.Length; j++)
                {
                    if (scenario.OriginalParameters[j].Name == parameters[i])
                        evaluation[i] = new Evaluation(scenario.OriginalParameters[j].ID, j, (int) weights[i]);
                }
            }
        }

        public static string[] NodeIDtoText(string history)
        {
            string[] nodes = history.Split(' ');
            string[] text = new string[nodes.Length];
            for(int i = 0; i < nodes.Length - 1; i++)
            {
                text[i] = scenario.FindStatement(nodes[i]).text.Value;   
            }
            return text;
        }

        private static string CreateControlString(statementType[] statements)
        {
            string controlString = statements[0].text.Value;
            for (int i = 1; i < statements.Length; i++)
            {
                if (statements[i] == null)
                    return controlString;
                controlString = controlString + " ";
                controlString = controlString + statements[i].text.Value;
            }
            return controlString;
        }

        public static Parameter[] OriginalParameters => scenario.OriginalParameters;
        public static Evaluation[] Evaluation => evaluation;
        public static int SubjectCount => scenario.SubjectCount;

        public static string ControlString
        {
            get { return controlString; }
            set { controlString = value; }
        }

        public static string GreedyControlString
        {
            get { return greedyControlString; }
            set { greedyControlString = value; }
        }
    }
}