﻿using System;
using System.Collections.Generic;
using ScenarioLanguage;

namespace PathFinder
{
    static class Helper
    {
        public static string[] StatementsToText(statementType[] statements)
        {
            string[] output = new string[statements.Length];
            for(int i = 0; i < statements.Length; i++)
            {
                if (statements[i] == null)
                    return output;
                output[i] = statements[i].text.Value;
            }
            return output;
        } 

        public static statementType[] IDsToStatements(string[] IDs, Scenario scenario)
        {
            statementType[] output = new statementType[IDs.Length];
            for (int i = 0; i < IDs.Length; i++)
            {
                if (IDs[i] == "")
                    return output;
                output[i] = scenario.FindStatement(IDs[i]);
            }
            return output;
        }

        public static string ArrayToString(string[] array)
        {
            string output = array[0];
            for(int i = 1; i < array.Length; i++)
            {
                output = output + " " + array[i];
            }
            return output;
        }
    }
}
