﻿using System;
using System.IO;
using System.Collections.Generic;
using ScenarioLanguage;

namespace PathFinder
{
    static class FileWriter
    {
        public static bool showAllOptimalPaths = false;

        private static string location;
        private static StreamWriter mainFile;
        private static StreamWriter parameterFile;

        public static bool windows = true;

        public static void NewFile()
        {
            string name = DateTime.Now.ToString();
            name = name.Replace(':', '-');
            if(windows)
                location = "output\\" + name;
            else
                location = "output/" + name;
            Directory.CreateDirectory(@location);
            if(windows)
                location += "\\";
            else
                location += "/";
            mainFile = new StreamWriter(@location + "main.txt");
            mainFile.AutoFlush = true;

        }

        public static void NewParameter(string parameterName)
        {
            parameterFile = new StreamWriter(@location + parameterName + ".txt");
            parameterFile.AutoFlush = true;
            OutputScenarioSmells(parameterName);
        }

        public static void WriteLine(string text)
        {
            mainFile.WriteLine(text);
        }

        public static void WriteHeader(string text)
        {
            WriteLine(" ");
            WriteLine("--------------------------------------------------");
            WriteLine("--------------------------------------------------");
            WriteLine(text);
            WriteLine("--------------------------------------------------");
            WriteLine("--------------------------------------------------");
            WriteLine(" ");
        }

        public static void OutputBestPath(List<statementType[]> optimalPaths, float bestScore, string name, SearchTree tree)
        {
            parameterFile.WriteLine(" ");
            parameterFile.WriteLine("##################################################");
            parameterFile.WriteLine(name);
            parameterFile.WriteLine("##################################################");
            parameterFile.WriteLine(" ");

            parameterFile.WriteLine("BEST SCORE: " + bestScore);
            parameterFile.WriteLine(" ");

            parameterFile.WriteLine("FINAL PARAMETER VALUES:");
            for(int i = 0; i < tree.ParameterCount; i++)
            {
                Parameter parameter = tree.FinalParameter(i);
                parameterFile.WriteLine(parameter.Name + ": " + parameter.Value);
            }
            parameterFile.WriteLine(" ");

            if (showAllOptimalPaths)
                OutputAllBestPaths(optimalPaths);
            else
                OutputOneBestPath(optimalPaths[0], optimalPaths.Count);
        }

        private static void OutputOneBestPath(statementType[] optimalPath, int bestPathCount)
        {
            parameterFile.WriteLine("AMOUNT OF BEST PATHS: " + bestPathCount);
            parameterFile.WriteLine(" ");
            parameterFile.WriteLine("BEST PATH: ");
            string[] optimalPathText = Helper.StatementsToText(optimalPath);
            for (int i = 0; i < optimalPathText.Length; i++)
                parameterFile.WriteLine(optimalPathText[i]);
        }

        private static void OutputAllBestPaths(List<statementType[]> optimalPaths)
        {
            parameterFile.WriteLine("BEST PATHS: ");
            for(int i = 0; i < optimalPaths.Count; i++)
            {
                string[] optimalPathText = Helper.StatementsToText(optimalPaths[i]);
                for (int j = 0; j < optimalPathText.Length; j++)
                    parameterFile.WriteLine(optimalPathText[j]);
                parameterFile.WriteLine(" ");
            }
        }

        public static void OutputScenarioSmells(string parameterName)
        {
            WriteLine(" ");
            WriteLine("##################################################");
            WriteLine(parameterName);
            WriteLine("##################################################");
            WriteLine(" ");
        }

        public static void OutputSmellOne(bool smell)
        {
            if (smell)
                WriteLine("Als de speler altijd de beste optie kiest, geeft dit soms niet de beste score");
            else
                WriteLine("Als de speler altijd de beste optie kiest, geeft dit ook de beste score");
        }

        public static void OutputsmellTwo(bool smell)
        {
            if (smell)
                WriteLine("Het langste pad geeft ook de hoogste score");
            else
                WriteLine("Het langste pad geeft niet de hoogste score");
        }

        public static void OutputsmellThree(bool smell)
        {
            if (smell)
                WriteLine("Het kortste pad geeft ook de laagste score");
            else
                WriteLine("Het kortste pad geeft niet de laagste score");
        }

        public static void OutputsmellFour(bool smell)
        {
            if (smell)
                WriteLine("Er is geen pad dat de minimum waarde geeft");
            else
                WriteLine("Er is een pad dat de minimum waarde geeft");
        }

        public static void OutputsmellFive(bool smell)
        {
            if (smell)
                WriteLine("Er is geen pad dat de maximum waarde geeft");
            else
                WriteLine("Er is een pad dat de maximum waarde geeft");
        }

    }
}
