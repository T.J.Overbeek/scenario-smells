﻿using System;
using System.Collections.Generic;
using ScenarioLanguage;

namespace PathFinder
{
    class BruteNode : Node
    {
        public BruteNode(SearchTree tree, statementType statement, Path path, BruteNode parent = null, int subjectIndex = 0) : base(tree, statement, path, parent, subjectIndex)
        {
            FindChildren();
        }

        protected override Node CreateChild(statementType childStatement, Path childPath, int childSubjectIndex)
        {
            if (CheckPreconditions(statement))
                return new BruteNode(tree, childStatement, childPath, this, childSubjectIndex);
            else
                return null;
        }

        protected override void FindWithinSubject()
        {
            int childCount = statement.responses.Length;
            for (int i = 0; i < childCount; i++)
            {
                string childID = statement.responses[i].idref;
                Node childNode = CreateChild(tree.FindStatement(childID, subjectIndex), path, subjectIndex);
            }
        }

        protected override void FindOutsideSubject()
        {
            List<SubjectStart> nextSubjects = path.AvailableSubjects;
            if (nextSubjects.Count == 0)
            {
                path = path.FindNextSubjects(tree.Scenario);
                nextSubjects = path.AvailableSubjects;
            }
            int childCount = 0;
            for (int i = 0; i < nextSubjects.Count; i++)
            {
                childCount += nextSubjects[i].ChildCount;
            }

            int counter = 0;
            for (int i = 0; i < nextSubjects.Count; i++)
            {
                Path newPath = path.SelectSubject(nextSubjects[i].ID);
                for (int j = 0; j < nextSubjects[i].ChildCount; j++)
                {
                    string childID = nextSubjects[i].StartStatements[j].id;
                    Node childNode = CreateChild(tree.FindStatement(childID, nextSubjects[i].ID), newPath, nextSubjects[i].ID);
                    counter++;
                }
            }
        }
    }
}