﻿using System;
using System.Collections.Generic;
using ScenarioLanguage;

namespace PathFinder
{
    class Subject
    {
        private dialogue dialogue;
        private dialogueStatements statements;
        private List<statementType> startStatements;

        public Subject(dialogue dialogue)
        {
            this.dialogue = dialogue;
            statements = dialogue.statements;
        }

        public statementType FindStatement(string id)
        {
            for (int i = 0; i < statements.Items.Length; i++)
            {
                if (statements.Items[i].id == id)
                    return statements.Items[i];
            }

            return null;
        }

        private void FindStartStatements()
        {
            startStatements = new List<ScenarioLanguage.statementType>();
            for(int i = 0; i < dialogue.starts.Length; i++)
            {
                startStatements.Add(FindStatement(dialogue.starts[i].idref));
            }
        }

        public List<statementType> StartStatements
        {
            get
            {
                if (startStatements == null)
                    FindStartStatements();
                return startStatements;
            }
        }
    }
}
