﻿using System;
using System.Collections.Generic;
using ScenarioLanguage;

namespace PathFinder
{
    struct Path
    {
        private bool[] subjectsChecked;
        private List<SubjectStart> availableSubjects;

        public void CreateEmptyPath()
        {
            subjectsChecked = new bool[Controller.SubjectCount];
            for (int i = 0; i < subjectsChecked.Length; i++)
                subjectsChecked[i] = false;
            subjectsChecked[0] = true;
            availableSubjects = new List<SubjectStart>();
        }

        public void CopyPath(Path path)
        {
            subjectsChecked = new bool[path.SubjectCount];
            for(int i = 0; i < path.SubjectCount; i++)
            {
                subjectsChecked[i] = path[i];
            }
            availableSubjects = new List<SubjectStart>(path.availableSubjects);
        }

        public Path SelectSubject(int subjectIndex)
        {
            Path newPath = new Path();
            newPath.CopyPath(this);
            newPath.subjectsChecked[subjectIndex] = true;

            int listID = -1;
            for(int i = 0; i < newPath.availableSubjects.Count; i++)
            {
                if (availableSubjects[i].ID == subjectIndex)
                    listID = i;
            }
            newPath.availableSubjects.RemoveAt(listID);
            return newPath;
        }

        public Path AddSubject(SubjectStart subject)
        {
            Path newPath = new Path();
            newPath.CopyPath(this);
            newPath.availableSubjects.Add(subject);
            return newPath;
        }

        public Path FindNextSubjects(Scenario scenario)
        {
            Path newPath = new Path();
            newPath.CopyPath(this);
            newPath.availableSubjects = scenario.FindStartStatements(this);
            return newPath;
        }

        public bool this[int index]
        {
            get { return subjectsChecked[index]; }
            set
            {
                if (value)
                    subjectsChecked[index] = value;
                else
                    OutputController.Error("Subjects can not be made false");
            }
        }

        public List<SubjectStart> AvailableSubjects
        {
            get
            {
                return availableSubjects;
            }
        }

        public int SubjectCount
        {
            get { return subjectsChecked.Length; }
        }
    }
}
