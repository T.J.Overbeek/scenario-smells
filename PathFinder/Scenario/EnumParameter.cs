﻿using System;
using System.Collections.Generic;
using ScenarioLanguage; 

namespace PathFinder
{
    struct EnumParameter : Parameter
    {
        string id;
        string name;
        int value;
        int minValue;
        int maxValue;
        bool minAndMax;
        string[] options;

        public EnumParameter(string id, string name, string[] options)
        {
            this.id = id;
            this.name = name;
            this.options = options;
            value = 0;
            minValue = 0;
            maxValue = 1;
            minAndMax = true;
        }

        public void SetValue(int value)
        {
            this.value = value;
            OutputController.ParameterCreation(id, name, value.ToString());
        }

        public void SetValue(string value)
        {
            for (int i = 0; i < options.Length; i++)
            {
                if (options[i] == value)
                   this.value = i;
            }
            OutputController.ParameterCreation(id, name, value.ToString());
        }

        public void ChangeValue(parameterEffect effect)
        {
            assignmentOperator @operator = effect.@operator;
            switch (@operator)
            {
                case assignmentOperator.assign:
                    SetValue(effect.value);
                    break;
                case assignmentOperator.addAssign:
                    OutputController.Error("Addition on Bool, not supported");
                    return;
                case assignmentOperator.subtractAssign:
                    OutputController.Error("Subtraction on Bool, not supported");
                    return;
            }

            if (maxValue != -1)
                if (value > maxValue)
                    value = maxValue;

            if (minValue != -1)
                if (value < minValue)
                    value = minValue;
        }

        public bool CompareValue(conditionsTypeCondition condition)
        {
            int compareValue = -1;
            for (int i = 0; i < options.Length; i++)
            {
                if (options[i] == condition.value)
                    this.value = i;
            }

            switch (condition.@operator)
            {
                case relationalOperator.equalTo:
                    return (value == compareValue);
                case relationalOperator.notEqualTo:
                    return (value != compareValue);
                case relationalOperator.greaterThan:
                    return (value > compareValue);
                case relationalOperator.greaterThanEqualTo:
                    return (value >= compareValue);
                case relationalOperator.lessThan:
                    return (value < compareValue);
                case relationalOperator.lessThanEqualTo:
                    return (value <= compareValue);
            }

            return false;
        }

        public Parameter Clone()
        {
            return (EnumParameter)this.MemberwiseClone();
        }

        public string ID => id;
        public string Name => name;
        public int Value => value;
        public int MinValue => minValue;
        public int MaxValue => maxValue;
        public bool MinAndMax => minAndMax;
    }
}