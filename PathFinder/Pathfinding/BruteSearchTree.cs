﻿using System;
using System.Collections.Generic;
using ScenarioLanguage;
using System.Diagnostics;

namespace PathFinder
{
    class BruteSearchTree : SearchTree
    {
        protected Node worstNode;
        protected List<Node> allWorstNodes;
        protected Node shortestNode;
        protected List<Node> allShortestNodes;
        protected Node longestNode;
        protected List<Node> allLongestNodes;

        protected bool[,] foundPreconditons;
        protected bool[,] foundParameters;

        public BruteSearchTree(Scenario scenario, Evaluation[] evaluation) : base(scenario, evaluation)
        {

        }

        public override void BuildTree()
        {
            stopwatch = Stopwatch.StartNew();
            OutputController.Debug("\nStarted building Search Tree");
            foundPreconditons = new bool[Controller.SubjectCount, OriginalParameters.Length];
            foundParameters = new bool[Controller.SubjectCount, OriginalParameters.Length];
            CreateStartNodes();
            OutputController.Debug("Finished building Search Tree \n");
            stopwatch.Stop();
        }

        protected override void CreateStartNodes()
        {
            Path path = new Path();
            path.CreateEmptyPath();

            string[] startIDs = Scenario.FindStartIDs();
            List<BruteNode> children = new List<BruteNode>();

            for (int i = 0; i < startIDs.Length; i++)
            {
                children.Add(new BruteNode(this, FindStatement(startIDs[i], 0), path));
            }
        }

        public override void ReportEndNode(Node endNode)
        {
            checkIfBestNode(endNode);
            checkIfWorstNode(endNode);
            checkIfLongest(endNode);
            checkIfShortest(endNode);
        }

        public void checkIfBestNode(Node endNode)
        {
            if (bestNode == null)
            {
                bestNode = endNode;
                allBestNodes = new List<Node>();
                allBestNodes.Add(endNode);
            }

            else if (EvaluateScore(endNode) == EvaluateScore(bestNode))
            {
                allBestNodes.Add(endNode);
            }

            else if (EvaluateScore(endNode) > EvaluateScore(bestNode))
            {
                bestNode = endNode;
                allBestNodes = new List<Node>();
                allBestNodes.Add(endNode);
            }
        }

        public void checkIfWorstNode(Node endNode)
        {
            if (worstNode == null)
            {
                worstNode = endNode;
                allWorstNodes = new List<Node>();
                allWorstNodes.Add(endNode);
            }

            else if (EvaluateScore(endNode) == EvaluateScore(worstNode))
            {
                allWorstNodes.Add(endNode);
            }

            else if (EvaluateScore(endNode) < EvaluateScore(worstNode))
            {
                worstNode = endNode;
                allWorstNodes = new List<Node>();
                allWorstNodes.Add(endNode);
            }
        }

        public void checkIfLongest(Node endNode)
        {
            if (longestNode == null)
            {
                longestNode = endNode;
                allLongestNodes = new List<Node>();
                allLongestNodes.Add(endNode);
            }

            else if (longestNode.PathLength == endNode.PathLength)
            {
                allLongestNodes.Add(endNode);
            }

            else if (longestNode.PathLength < endNode.PathLength)
            {
                longestNode = endNode;
                allLongestNodes = new List<Node>();
                allLongestNodes.Add(endNode);
            }
        }

        public void checkIfShortest(Node endNode)
        {
            if (shortestNode == null)
            {
                shortestNode = endNode;
                allShortestNodes = new List<Node>();
                allShortestNodes.Add(endNode);
            }

            else if (shortestNode.PathLength == endNode.PathLength)
            {
                allShortestNodes.Add(endNode);
            }

            else if (shortestNode.PathLength > endNode.PathLength)
            {
                shortestNode = endNode;
                allShortestNodes = new List<Node>();
                allShortestNodes.Add(endNode);
            }
        }

        public void reportPrecondition(int subjectIndex, int parameterIndex)
        {
            foundPreconditons[subjectIndex, parameterIndex] = true;
        }

        public void reportParameter(int subjectIndex, int parameterIndex)
        {
            foundParameters[subjectIndex, parameterIndex] = true;
        }

        public List<statementType[]> AllWorstPaths
        {
            get
            {
                int worstPathCount = allWorstNodes.Count;
                List<statementType[]> worstPaths = new List<statementType[]>();
                for (int i = 0; i < worstPathCount; i++)
                {
                    worstPaths.Add(Helper.IDsToStatements(allWorstNodes[i].history.Split(' '), scenario));
                }
                return worstPaths;
            }
        }

        public List<statementType[]> AllLongestPaths
        {
            get
            {
                int longestPathCount = allLongestNodes.Count;
                List<statementType[]> longestPaths = new List<statementType[]>();
                for (int i = 0; i < longestPathCount; i++)
                {
                    longestPaths.Add(Helper.IDsToStatements(allLongestNodes[i].history.Split(' '), scenario));
                }
                return longestPaths;
            }
        }

        public List<statementType[]> AllShortestPaths
        {
            get
            {
                int shortestPathCount = allShortestNodes.Count;
                List<statementType[]> shortestPaths = new List<statementType[]>();
                for (int i = 0; i < shortestPathCount; i++)
                {
                    shortestPaths.Add(Helper.IDsToStatements(allShortestNodes[i].history.Split(' '), scenario));
                }
                return shortestPaths;
            }
        }

        public float WorstScore
        {
            get { return EvaluateScore(worstNode); }
        }

        public int LongestPathLength
        {
            get { return longestNode.PathLength; }
        }

        public float[] LongestPathScores
        {
            get
            {
                float[] scores = new float[allLongestNodes.Count];
                for (int i = 0; i < allLongestNodes.Count; i++)
                    scores[i] = EvaluateScore(allLongestNodes[i]);
                return scores;
            }
        }

        public int ShortestPath
        {
            get { return shortestNode.PathLength; }
        }

        public float[] ShortestPathScores
        {
            get
            {
                float[] scores = new float[allShortestNodes.Count];
                for (int i = 0; i < allShortestNodes.Count; i++)
                    scores[i] = EvaluateScore(allShortestNodes[i]);
                return scores;
            }
        }

        public bool SubjectMonotone(int index)
        {
            int subjectIndex = -1;
            int subjectCount = 0;
            for (int i = 0; i < foundPreconditons.GetLength(0); i++)
            {
                if(foundPreconditons[i, index])
                {
                    subjectIndex = i;
                    subjectCount++;
                }
            }

            if (subjectCount == 0)
                return true;
            if (subjectCount > 1)
                return false;

            for (int j = 0; j < foundParameters.GetLength(0); j++)
            {
                if(foundParameters[j, index])
                {
                    if (subjectIndex != j)
                        return false;
                }
            }

            return true;
        }
    }
}
