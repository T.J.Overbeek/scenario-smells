﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace PathFinder
{
    class CorrelationSearchTree : SearchTree
    {
        public List<Parameter[]> finalParameters; 

        public CorrelationSearchTree(Scenario scenario, Evaluation[] evaluation) : base(scenario, evaluation)
        {
            finalParameters = new List<Parameter[]>();
        }

        public override void BuildTree()
        {
            stopwatch = Stopwatch.StartNew();
            OutputController.Debug("\nStarted building Search Tree");
            CreateStartNodes();
            OutputController.Debug("Finished building Search Tree \n");
            stopwatch.Stop();
        }

        protected override void CreateStartNodes()
        {
            Path path = new Path();
            path.CreateEmptyPath();

            string[] startIDs = Scenario.FindStartIDs();
            List<BruteNode> children = new List<BruteNode>();

            for (int i = 0; i < startIDs.Length; i++)
            {
                children.Add(new BruteNode(this, FindStatement(startIDs[i], 0), path));
            }
        }

        public override void ReportEndNode(Node endNode)
        {
            finalParameters.Add(endNode.parameters);
        }

        public double CalculateCorrelation(int first, int second)
        {
            Int64 totalSum = 0;
            Int64 firstSum = 0;
            Int64 secondSum = 0;
            decimal firstSquireSum = 0;
            decimal secondSquireSum = 0;

            for (int i = 0; i < finalParameters.Count; i++)
            {
                totalSum += (finalParameters[i][first].Value * finalParameters[i][second].Value);
                firstSum += finalParameters[i][first].Value;
                secondSum += finalParameters[i][second].Value;
                firstSquireSum += (finalParameters[i][first].Value * finalParameters[i][first].Value);
                secondSquireSum += (finalParameters[i][second].Value * finalParameters[i][second].Value);
            }

            int count = finalParameters.Count;
            Int64 test = count * totalSum;
            Int64 correlationTop = (finalParameters.Count * totalSum) - (firstSum * secondSum);
            decimal correlationBottomOne = finalParameters.Count * firstSquireSum - (firstSum * firstSum);
            decimal correlationBottomTwo = finalParameters.Count * secondSquireSum - (secondSum * secondSum);
            decimal correlationBottom = Sqrt(correlationBottomOne) * Sqrt(correlationBottomTwo);
            decimal correlation = 0;
            if (correlationTop != correlationBottom)
                correlation = correlationTop / correlationBottom;

            return (double)correlation;
        }

        public static decimal Sqrt(decimal x, decimal epsilon = 0.0M)
        {
            if (x < 0) throw new OverflowException("Cannot calculate square root from a negative number");

            decimal current = (decimal)Math.Sqrt((double)x), previous;
            do
            {
                previous = current;
                if (previous == 0.0M) return 0;
                current = (previous + x / previous) / 2;
            }
            while (Math.Abs(previous - current) > epsilon);
            return current;
        }
    }
}
