﻿using System;
using System.Collections.Generic;
using ScenarioLanguage;

namespace PathFinder
{
    struct BoolParameter : Parameter
    {
        string id;
        string name;
        int value;
        int minValue;
        int maxValue;
        bool minAndMax;

        public BoolParameter(string id, string name)
        {
            this.id = id;
            this.name = name;
            value = 0;
            minValue = 0;
            maxValue = 1;
            minAndMax = true;
        }

        public void SetValue(int value)
        {
            this.value = value;
            OutputController.ParameterCreation(id, name, value.ToString());
        }

        public void SetValue(string value)
        {
            switch(value)
            {
                case "true": this.value = 1;
                    break;
                case "false": this.value = 0;
                    break;
            }
            OutputController.ParameterCreation(id, name, value.ToString());
        }

        public void ChangeValue(parameterEffect effect)
        {
            assignmentOperator @operator = effect.@operator;
            switch (@operator)
            {
                case assignmentOperator.assign:
                    SetValue(effect.value);
                    break;
                case assignmentOperator.addAssign:
                    OutputController.Error("Addition on Bool, not supported");
                    return;
                case assignmentOperator.subtractAssign:
                    OutputController.Error("Subtraction on Bool, not supported");
                    return;
            }

            if (maxValue != -1)
                if (value > maxValue)
                    value = maxValue;

            if (minValue != -1)
                if (value < minValue)
                    value = minValue;
        }

        public bool CompareValue(conditionsTypeCondition condition)
        {
            int compareValue = -1;
            switch (condition.value)
            {
                case "true":
                    compareValue = 1;
                    break;
                case "false":
                    compareValue = 0;
                    break;
            }

            switch (condition.@operator)
            {
                case relationalOperator.equalTo:
                    return (value == compareValue);
                case relationalOperator.notEqualTo:
                    return (value != compareValue);
                case relationalOperator.greaterThan:
                    return (value > compareValue);
                case relationalOperator.greaterThanEqualTo:
                    return (value >= compareValue);
                case relationalOperator.lessThan:
                    return (value < compareValue);
                case relationalOperator.lessThanEqualTo:
                    return (value <= compareValue);
            }

            return false;
        }

        public Parameter Clone()
        {
            return (BoolParameter)this.MemberwiseClone();
        }

        public string ID => id;
        public string Name => name;
        public int Value => value;
        public int MinValue => minValue;
        public int MaxValue => maxValue;
        public bool MinAndMax => minAndMax;
    }
}