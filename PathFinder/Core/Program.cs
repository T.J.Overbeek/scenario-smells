﻿using System;
using System.Collections.Generic;
using ScenarioLanguage;

namespace PathFinder
{
    class Program
    {
        private static bool running = true;
        private enum TestCases {Parameter, Longest, Shortest, TotalScore, CompleteAnalysis, Correlation, None};
        private static TestCases testCase = TestCases.None;
        private static string fileLocation = "C:\\Users\\";

        static void Main(string[] args)
        {
            if (testCase == TestCases.None)
            {
                while (running)
                {
                    ReadInput();
                }
            }
            else
            {
                switch(testCase)
                {
                    case TestCases.Parameter:
                        TestCaseParameter();
                        break;
                    case TestCases.Longest:
                        TestCaseLongest();
                        break;
                    case TestCases.Shortest:
                        TestCaseShortest();
                        break;
                    case TestCases.TotalScore:
                        TestCaseTotalScore();
                        break;
                    case TestCases.CompleteAnalysis:
                        TestCaseCompleteAnalysis();
                        break;
                    case TestCases.Correlation:
                        CorrelationTest();
                        break;
                }
                ReadInput();
            }
        }

        private static void ReadInput()
        {
            string input = Console.ReadLine();
            string[] inputSplit = input.Split(' ');
            switch (inputSplit[0])
            {
                case "complete":
                    CompleteEvaluation(inputSplit);
                    OutputController.Output("Evaluation Complete");
                    break;
                case "bruteOnly":
                    CompleteEvaluation(inputSplit, false);
                    OutputController.Output("Evaluation Complete");
                    break;
                case "short":
                    ShortEvaluation(inputSplit);
                    OutputController.Output("Evaluation Complete");
                    break;
            }
        }

        private static void CompleteEvaluation(string[] input, bool gready = true)
        {
            string scenarioName = input[1];
            Controller.InitializeScenario(scenarioName);
            string[] parameters;
            float[] weights;
            if(input.Length == 3)
            {
                parameters = new string[1];
                parameters[0] = input[2];
                weights = new float[1];
                weights[0] = 1;
            }
            else
            {
                int parameterLength = input.Length - 2;
                if(parameterLength%2 != 0)
                    OutputController.Error("ERROR: not the right amount of parameters");
                int parameterCount = parameterLength / 2;
                parameters = new string[parameterCount];
                weights = new float[parameterCount];
                for (int i = 0; i < parameterCount; i++)
                {
                    parameters[i] = input[2 + i * 2];
                    weights[i] = float.Parse(input[3 + i * 2]);
                }
            }
            Controller.CompleteEvaluation(parameters, weights, gready);
        }

        private static void ShortEvaluation(string[] input)
        {
            string scenarioName = input[1];
            Controller.InitializeScenario(scenarioName);
            string[] parameters;
            float[] weights;
            if (input.Length == 3)
            {
                parameters = new string[1];
                parameters[0] = input[2];
                weights = new float[1];
                weights[0] = 1;
            }
            else
            {
                int parameterLength = input.Length - 2;
                if (parameterLength % 2 != 0)
                    OutputController.Error("ERROR: not the right amount of parameters");
                int parameterCount = parameterLength / 2;
                parameters = new string[parameterCount];
                weights = new float[parameterCount];
                for (int i = 0; i < parameterCount; i++)
                {
                    parameters[i] = input[2 + i * 2];
                    weights[i] = float.Parse(input[3 + i * 2]);
                }
            }
            Controller.ShortEvaluation(parameters, weights);
        }

        private static void DetermineEvaluationParameters(string[] inputSplit)
        {
            if (inputSplit.Length == 2)
            {
                Controller.ChangeEvaluation(inputSplit[2]);
            }
            else
            {
                int count = inputSplit.Length - 2;
                if (count%2 != 0)
                    OutputController.Error("ERROR: not the right amount of parameters");
                string[] parameters = new string[count];
                for(int i = 0; i < count; i++)
                {
                    parameters[i] = inputSplit[i + 2];
                }
                Controller.ChangeEvaluation(parameters);
            }
        }

        private static void TestCaseParameter()
        {
            OutputController.Output("Running Test Case 1W:");
            OutputController.DebugMode = false;
            Controller.InitializeScenario(fileLocation + "Test3M.xml");

            Controller.ChangeEvaluation("Score1");
            string controlString = "C101 P101 C102 P104 C105 P108 C108 P203 C203 P207 C206 P2011 C209 P303 C303 P307 C306 P311 C309 P403 C403 P407 C406 P411 C409 P601 C601 P604 C604 P608 C607";
            Controller.WorstPathTest(controlString);
        }

        private static void TestCaseLongest()
        {
            OutputController.Output("Running Test Case 1L:");
            OutputController.DebugMode = false;
            Controller.InitializeScenario(fileLocation + "Test3M.xml");
            Controller.LongestPathTest(33);
        }

        private static void TestCaseShortest()
        {
            OutputController.Output("Running Test Case 1S:");
            OutputController.DebugMode = false;
            Controller.InitializeScenario(fileLocation + "Test3M.xml");
            Controller.ShortestPathTest(2);
        }

        private static void TestCaseTotalScore()
        {
            OutputController.Output("Running Test Case 1M");
            OutputController.Output(" ");
            OutputController.DebugMode = false;
            Controller.InitializeScenario(fileLocation + "Test3M.xml");

            OutputController.Output("Running Total Score: ");
            Controller.ChangeEvaluation(new string[] { "Score1", "1", "Score2", "1" });
            string controlString = "C101 P103 C104 P107 C107 P111 C110 P401 C401 P404 C404 P408 C407 P301 C301 P304 C304 P308 C307 P201 C201 P204 C204 P208 C207 P602 C602 P606 C605 P609 C608";
            Controller.BestPathTest(controlString);

            OutputController.Output(" ");

            OutputController.Output("Running Score 1:");
            Controller.ChangeEvaluation("Score1");
            controlString = "C101 P103 C104 P107 C107 P111 C110 P401 C401 P404 C404 P408 C407 P301 C301 P304 C304 P308 C307 P201 C201 P204 C204 P208 C207 P602 C602 P606 C605 P610 C608";
            Controller.BestPathTest(controlString);

            OutputController.Output(" ");

            OutputController.Output("Running Score 2:");
            Controller.ChangeEvaluation("Score2");
            controlString = "C101 P101 C102 P104 C105 P108 C108 P401 C401 P404 C404 P408 C407 P301 C301 P304 C304 P308 C307 P201 C201 P204 C204 P208 C207 P601 C601 P604 C604 P609 C608";
            Controller.BestPathTest(controlString);
        }

        private static void TestCaseCompleteAnalysis()
        {
            OutputController.Output("Running Test Case 1M");
            OutputController.Output(" ");
            OutputController.DebugMode = false;
            Controller.InitializeScenario(fileLocation + "Test3M.xml");

            OutputController.Output("Running Total Score Best Path: ");
            Controller.ChangeEvaluation(new string[] { "Score1", "1", "Score2", "1" });
            string controlString = "C101 P103 C104 P107 C107 P111 C110 P401 C401 P404 C404 P408 C407 P301 C301 P304 C304 P308 C307 P201 C201 P204 C204 P208 C207 P602 C602 P606 C605 P609 C608";
            Controller.BestPathTest(controlString);

            OutputController.Output(" ");

            OutputController.Output("Running Total Score Greedy Path: ");
            controlString = "C101 P103 C104 P107 C107 P111 C110 P301 C301 P305 C305 P309 C308 P201 C201 P205 C205 P209 C208 P401 C401 P404 C404 P408 C407 P602 C602 P606 C605 P609 C608";
            Controller.GreedyPathTest(controlString);

            OutputController.Output(" ");

            OutputController.Output("Running Score 1 Best Path:");
            Controller.ChangeEvaluation("Score1");
            controlString = "C101 P103 C104 P107 C107 P111 C110 P401 C401 P404 C404 P408 C407 P301 C301 P304 C304 P308 C307 P201 C201 P204 C204 P208 C207 P602 C602 P606 C605 P610 C608";
            Controller.BestPathTest(controlString);

            OutputController.Output(" ");

            OutputController.Output("Running Score 1 Greedy Path:");
            controlString = "C101 P103 C104 P107 C107 P111 C110 P301 C301 P305 C305 P309 C308 P201 C201 P205 C205 P209 C208 P401 C401 P404 C404 P408 C407 P602 C602 P606 C605 P610 C608";
            Controller.GreedyPathTest(controlString);

            OutputController.Output(" ");

            OutputController.Output("Running Score 2 Best Path:");
            Controller.ChangeEvaluation("Score2");
            controlString = "C101 P101 C102 P104 C105 P108 C108 P401 C401 P404 C404 P408 C407 P301 C301 P304 C304 P308 C307 P201 C201 P204 C204 P208 C207 P601 C601 P604 C604 P609 C608";
            Controller.BestPathTest(controlString);

            OutputController.Output(" ");

            OutputController.Output("Running Score 2 Greedy Path:");
            controlString = "C101 P101 C102 P104 C105 P108 C108 P301 C301 P305 C305 P309 C308 P201 C201 P205 C205 P209 C208 P401 C401 P404 C404 P408 C407 P601 C601 P604 C604 P609 C608";
            Controller.GreedyPathTest(controlString);
        }

        private static void CorrelationTest()
        {
            OutputController.Output("Running Correlation Test:");
            Controller.InitializeScenario(fileLocation + "Test3M.xml");
            Controller.ChangeEvaluation(new string[] { "Score1", "1", "Score2", "1", "GateSubject3", "1"});
            Controller.Correlation();
        }
    }
}