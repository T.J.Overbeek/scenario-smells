﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathFinder
{
    struct Evaluation
    {
        private string id;
        private int index;
        private int weight;

        public Evaluation(string id, int index, int weight = 1)
        {
            this.id = id;
            this.index = index;
            this.weight = weight;
        }

        public string ID => id;
        public int Index => index;
        public int Weight => weight;
    }
}
