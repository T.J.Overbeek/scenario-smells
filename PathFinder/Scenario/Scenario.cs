﻿using System;
using System.Collections.Generic;
using ScenarioLanguage;

namespace PathFinder
{
    class Scenario
    {
        private ScenarioLanguage.scenario scenarioXML;
        private dialogue[][] sequence;
        private Subject[][] subjects;
        private SubjectID[] subjectIDs;
        private Parameter[] parameters;
        private int subjectCount;

        public Scenario(ScenarioLanguage.scenario scenarioXML)
        {
            this.scenarioXML = scenarioXML;
            subjectCount = 0;
            sequence = scenarioXML.sequence;
            subjects = new Subject[sequence.Length][];
            for(int i = 0; i < sequence.Length; i++)
            {
                subjects[i] = new Subject[sequence[i].Length];
                for(int j = 0; j < sequence[i].Length; j++)
                {
                    subjects[i][j] = new Subject(sequence[i][j]);
                    subjectCount++;
                }
            }

            subjectIDs = new SubjectID[subjectCount];
            int counter = 0;
            for(int i = 0; i < subjects.Length; i++)
            {
                for(int j = 0; j < subjects[i].Length; j++)
                {
                    subjectIDs[counter] = new SubjectID(counter, i, j);
                    counter++;
                }
            }
            InitializeParameters();
        }

        private void InitializeParameters()
        {
            definitionType[] parameterDefinitions = scenarioXML.definitions.parameters.userDefined;
            parameters = new Parameter[parameterDefinitions.Length];
 
            for (int i = 0; i < parameters.Length; i++)
            {
                domainDataTypeTypeInteger integerType = parameterDefinitions[i].type.Item as domainDataTypeTypeInteger;
                domainDataTypeTypeBoolean boolType = parameterDefinitions[i].type.Item as domainDataTypeTypeBoolean;
                domainDataTypeTypeEnumeration enumType = parameterDefinitions[i].type.Item as domainDataTypeTypeEnumeration;

                if (integerType != null)
                    InitializeInteger(parameterDefinitions[i], i);
                if (boolType != null)
                    InitializeBool(parameterDefinitions[i], i);
                if (enumType != null)
                    InitializeEnum(parameterDefinitions[i], i);
            }

            scenarioMetadataInitialParameterValues initialParameters = scenarioXML.metadata.initialParameterValues;
            for (int j = 0; j < parameters.Length; j++)
            {
                simpleParameterValuesParameterValue initialParameter = initialParameters.userDefined[j] as simpleParameterValuesParameterValue;
                for(int k = 0; k < parameters.Length; k++)
                {
                    if(parameters[k].ID == initialParameter.idref)
                    {
                        parameters[k].SetValue(initialParameter.initialValue);
                    }
                }
            }
        }

        private void InitializeInteger(definitionType parameterDefinition, int index)
        {
            domainDataTypeTypeInteger integerType = parameterDefinition.type.Item as domainDataTypeTypeInteger;
            if (integerType.minimumSpecified)
                parameters[index] = new IntegerParameter(parameterDefinition.id, parameterDefinition.name, integerType.minimum, integerType.maximum);
            else
                parameters[index] = new IntegerParameter(parameterDefinition.id, parameterDefinition.name);
        }

        public void InitializeBool(definitionType parameterDefinition, int index)
        {
            domainDataTypeTypeBoolean boolType = parameterDefinition.type.Item as domainDataTypeTypeBoolean;
            parameters[index] = new BoolParameter(parameterDefinition.id, parameterDefinition.name);
        }

        public void InitializeEnum(definitionType parameterDefinition, int index)
        {
            domainDataTypeTypeEnumeration enumType = parameterDefinition.type.Item as domainDataTypeTypeEnumeration;
            parameters[index] = new EnumParameter(parameterDefinition.id, parameterDefinition.name, enumType.option);
        }

        public void InitializeEvaluation()
        {
            Controller.ChangeEvaluation(parameters[0].Name);
        }

        public statementType FindStatement(string id)
        {
            foreach(SubjectID subjectID in subjectIDs)
            {
                statementType statement = subjects[subjectID.x][subjectID.y].FindStatement(id);
                if (statement != null)
                    return statement;
            }
            return null;
        }

        public statementType FindStatement(string id, int subjectIndex)
        {
            SubjectID subjectID = subjectIDs[subjectIndex];
            return subjects[subjectID.x][subjectID.y].FindStatement(id);
        }

        public List<SubjectStart> FindStartStatements(Path path)
        {
            List<SubjectStart> subjectStarts = new List<SubjectStart>();
            int counter = 0;
            bool foundRow = false;
            for (int i = 0; i < sequence.Length; i++)
            {
                for (int j = 0; j < sequence[i].Length; j++)
                {
                    if (!path[counter])
                    {
                        if (!foundRow && j != 0)
                            OutputController.Error("Incorrect row change");
                        foundRow = true;
                        SubjectStart foundSubject = new SubjectStart(counter, subjects[i][j].StartStatements);
                        subjectStarts.Add(foundSubject);
                    }
                    counter++;
                }
                if (foundRow)
                    break;
            }
            return subjectStarts;
        }

        public Parameter FindParameter(string id)
        {
            for (int i = 0; i < parameters.Length; i++)
            {
                if (parameters[i].ID == id)
                    return parameters[i];
            }
            OutputController.Error("Parameter not found! Findings may be wrong!");
            return parameters[0];
        }

        public string[] FindStartIDs()
        {
            string[] result = new string[sequence[0][0].starts.Length];
            for(int i = 0; i < result.Length; i++)
            {
               result[i] = sequence[0][0].starts[i].idref;
            }
            return result;
        }

        public Parameter[] OriginalParameters => parameters;
        public string StartID => sequence[0][0].starts[0].idref;
        public int SubjectCount => subjectCount;
    }

    struct SubjectID
    {
        public readonly int idref;
        public readonly int x;
        public readonly int y;

        public SubjectID(int idref, int x, int y)
        {
            this.idref = idref;
            this.x = x;
            this.y = y;
        }
    }
}
