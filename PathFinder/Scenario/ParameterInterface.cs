﻿using System;
using System.Collections.Generic;
using ScenarioLanguage;

namespace PathFinder
{
    public interface Parameter
    {
        void SetValue(int value);
        void SetValue(string value);
        void ChangeValue(parameterEffect effect);
        bool CompareValue(conditionsTypeCondition condition);
        Parameter Clone();

        string ID { get;}
        string Name { get;}
        int Value { get;}
        int MinValue { get;}
        int MaxValue { get;}
        bool MinAndMax { get;}
    }
}
