﻿using System;
using System.Collections.Generic;
using ScenarioLanguage;
using System.Diagnostics;

namespace PathFinder
{
    abstract class SearchTree
    {
        protected Scenario scenario;
        protected Evaluation[] evaluation;
        protected Stopwatch stopwatch;
        protected Node bestNode;
        protected List<Node> allBestNodes;
        
        public SearchTree(Scenario scenario, Evaluation[] evaluation)
        {
            this.scenario = scenario;
            this.evaluation = evaluation;
        }

        public float EvaluateScore(Node node)
        {
            float score = 0;
            float totalWeight = 0;
            for (int i = 0; i < evaluation.Length; i++)
            {
                int index = evaluation[i].Index;
                score += node.Parameter(index).Value * evaluation[i].Weight;
                totalWeight += evaluation[i].Weight;
            }
            return score / totalWeight;
        }

        public abstract void BuildTree();
        public abstract void ReportEndNode(Node endNode);
        protected abstract void CreateStartNodes();

        public statementType FindStatement(string id, int subjectIndex)
        {
            return scenario.FindStatement(id, subjectIndex);
        }

        public Scenario Scenario
        {
            get { return scenario; }
        }

        public Parameter[] OriginalParameters
        {
            get { return scenario.OriginalParameters; }
        }

        public statementType[] BestPath
        {
            get { return Helper.IDsToStatements(bestNode.history.Split(' '), scenario); }
        }

        public List<statementType[]> AllBestPaths
        {
            get
            {
                int bestPathCount = allBestNodes.Count;
                List<statementType[]> bestPaths = new List<statementType[]>();
                for (int i = 0; i < bestPathCount; i++)
                {
                    bestPaths.Add(Helper.IDsToStatements(allBestNodes[i].history.Split(' '), scenario));
                }
                return bestPaths;
            }
        }

        public Parameter FinalParameter(int index)
        {
            return bestNode.Parameter(index);
        }


        public int BestPathCount => allBestNodes.Count;
        public float BestScore => EvaluateScore(bestNode);
        public long CalculationTime => stopwatch.ElapsedMilliseconds;
        public int ParameterCount => OriginalParameters.Length;
    }
}