﻿using System;
using System.Collections.Generic;
using ScenarioLanguage;

namespace PathFinder
{
    static class OutputController
    {
        private static bool debug = false;

        public static void Output(string output)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(output);
        }

        public static void Debug(string output)
        {
            if (!debug)
                return;

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(output); 
        }

        public static void Error(string output)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("ERROR: " + output);
        }

        public static void Error(string output, string nodeText)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("ERROR: " + output + ", NODE: " + nodeText);
        }

        public static void ParameterCreation(string id, string name, string value)
        {
            string output = "Created Parameter \t id: " + id + "\t name: " + name + "\t value: " + value;
            Debug(output);
        }

        public static void NodeCreation(string id, string text)
        {
            string output = "Created Node \t id: " + id + "\t text: " + text;
            Debug(output);
        }

        public static void OutputBestPath(statementType[] optimalPath, float score, long calculationTime)
        {
            string outputOptimalPath = Helper.ArrayToString(Helper.StatementsToText(optimalPath));
            Output("Optimal Path: " + outputOptimalPath);
            Output("Best Score: " + score);
            Output("Calculation Time: " + calculationTime + " ms");
        }

        public static bool DebugMode
        {
            get { return debug; }
            set { debug = value; }
        }
    }
}
