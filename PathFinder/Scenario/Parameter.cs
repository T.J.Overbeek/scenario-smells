﻿using System;
using System.Collections.Generic;
using ScenarioLanguage;

namespace PathFinder
{
    struct IntegerParameter : Parameter
    {
        string id;
        string name;
        int value;
        int minValue;
        int maxValue;
        bool minAndMax;

        public IntegerParameter(string id, string name)
        {
            this.id = id;
            this.name = name;
            value = 0;
            minValue = -1;
            maxValue = -1;
            minAndMax = false;
        }

        public IntegerParameter(string id, string name, int minValue, int maxValue)
        {
            this.id = id;
            this.name = name;
            value = 0;
            this.minValue = minValue;
            this.maxValue = maxValue;
            minAndMax = true;
        }

        public void SetValue(int value)
        {
            this.value = value;
            OutputController.ParameterCreation(id, name, value.ToString());
        }

        public void SetValue(string value)
        {
            SetValue(int.Parse(value));
        }

        public void ChangeValue(parameterEffect effect)
        {
            assignmentOperator @operator = effect.@operator;
            switch(@operator)
            {
                case assignmentOperator.assign:
                    SetValue(int.Parse(effect.value));
                    break;
                case assignmentOperator.addAssign:
                    value += int.Parse(effect.value);
                    break;
                case assignmentOperator.subtractAssign:
                    value -= int.Parse(effect.value);
                    break;
            }

            if (maxValue != -1)
                if (value > maxValue)
                    value = maxValue;

            if (minValue != -1)
                if (value < minValue)
                    value = minValue;
        }

        public bool CompareValue(conditionsTypeCondition condition)
        {
            int compareValue = int.Parse(condition.value);
            switch(condition.@operator)
            {
                case relationalOperator.equalTo:
                    return (value == compareValue);
                case relationalOperator.notEqualTo:
                    return (value != compareValue);
                case relationalOperator.greaterThan:
                    return (value > compareValue);
                case relationalOperator.greaterThanEqualTo:
                    return (value >= compareValue);
                case relationalOperator.lessThan:
                    return (value < compareValue);
                case relationalOperator.lessThanEqualTo:
                    return (value <= compareValue);
            }

            return false;
        }

        public Parameter Clone()
        {
            return (IntegerParameter) this.MemberwiseClone();
        }

        public string ID => id;
        public string Name => name;
        public int Value => value;
        public int MinValue => minValue;
        public int MaxValue => maxValue;
        public bool MinAndMax => minAndMax;
    }
}